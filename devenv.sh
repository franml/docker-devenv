#!/bin/bash

# TODO:
#   - move shared folders config to 'configure', right after forwarding the ports. Else shared folders disappear..
#   - automatically input password 'tcuser' after boot2docker ssh

# Get Environment variables
source `dirname $0`/cx_env

running () {
  isrunning=$(boot2docker status | grep -ciE "running")
}

unmap () {
  if [ $isrunning -eq "1" ]; then
    boot2docker stop
  fi
  for i in $CX_NG_PORT $CX_API_PORT $CX_MYSQL_PORT $CX_REDIS_PORT
  do
    echo "Unmapping port $i from boot2docker-vm";
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "tcp-port$i" 2>/dev/null;
    VBoxManage modifyvm "boot2docker-vm" --natpf1 delete "udp-port$i" 2>/dev/null;
  done
  if [ $running -eq "1" ]; then
    boot2docker start
  fi
}

installNg () {
  docker run -v /srv/www/ng:/srv/www/ng ngd bash 'cd /srv/www/ng && npm install && bower install'
  docker commit $(docker ps -l -q) ngd
}

case "$1" in
  test)
    ;;
  install)
    #boot2docker delete | download | init
    case "$2" in
      fresh)
        running
        if [ $isrunning -eq "0" ]; then
          boot2docker start
        fi
        sh ./boot2docker/build_docker.sh
        yes | cp -rf ./boot2docker/boot2docker.iso $HOME/.boot2docker/boot2docker.iso
      ;;
      copy)
        yes | cp -rf ./boot2docker/boot2docker.iso $HOME/.boot2docker
      ;;
    esac
    boot2docker stop
    VBoxManage sharedfolder add boot2docker-vm -name cloud-ng -hostpath $CX_NG_FOLDER
    VBoxManage sharedfolder add boot2docker-vm -name cloud-api -hostpath $CX_API_FOLDER
    boot2docker start
    boot2docker ssh "sudo mkdir -p /srv/www/ng; sudo mkdir -p /srv/www/api; sudo modprobe vboxsf && sudo mount -t vboxsf cloud-ng /srv/www/ng && sudo mount -t vboxsf cloud-api /srv/www/api"
    # pass: tcuser
    ;;
  build)
    running
    if [ $isrunning -eq "0" ]; then
      boot2docker start
    fi
    # docker build -t ngd ./docker/ng && installNg
    docker build -t ngd ./docker/ng
    # docker build -t apid docker/api
    # docker build -t mysqld docker/mysql
    # docker build -t redisd docker/redis
    ;;
  configure)
    boot2docker stop
    for i in $CX_NG_PORT $CX_API_PORT $CX_MYSQL_PORT $CX_REDIS_PORT
    do
      busy=$(netstat -an | grep $i | grep -ciE "(LISTEN|ESTABLISHED)")
      if [ $busy -eq "1" ]; then
        echo "Port $i is already in use. Check which process is using it with lsof -i :$i"
        unmap
        exit 1
      fi;
      echo "Forwarding port $i on boot2docker-vm";
      VBoxManage modifyvm "boot2docker-vm" --natpf1 "tcp-port$i,tcp,,$i,,$i" 2>/dev/null;
      VBoxManage modifyvm "boot2docker-vm" --natpf1 "udp-port$i,udp,,$i,,$i" 2>/dev/null;
    done
    echo "All port forwarding setup successfully";
    boot2docker start
    ;;
  unmap)
    unmap
    ;;
  start)
    # docker rm -f $(docker ps -a -q)
    # docker run -d -p $CX_REDIS_PORT:6379 --name="redis" redisd
    # docker run -d -p $CX_MYSQL_PORT:3306 --name="mysql" mysqld
    # docker run -d -p $CX_API_PORT:8080 --name="api" apid
    # docker run -d -p $CX_NG_PORT:8000 --name="ng" ngd
    docker run -d -v /srv/www/ng:/srv/www/ng -p $CX_NG_PORT:8000 --name="ng" ngd sh -c "cd /srv/www/ng && grunt watch"
    # docker run -d -p $CX_NG_PORT:8000 --name ng -v /srv/www/ng:/srv/www/ng ngd
    ;;
  update)
    case $2 in
      ng)
        echo "Now we would do bower update"
        # docker run a5f142a604b3 apt-get install -y memcached
        # docker commit 9fb69e798e67
        ;;
      api)
        echo "Now we would do php composer.phar update"
        ;;
    esac
    ;;
  delconts)
    docker rm -f $(docker ps -a | cut -d' ' -f1 | tail -n +2);
    ;;
  delimgs)
    echo -n "Are you sure? (y/n) "
    read yesno < /dev/tty
    if [ "x$yesno" = "xy" ]; then
      docker rmi $(docker images | grep -E "ngd|apid|mysqld|redisd" | tr -s ' ' | cut -d' ' -f3)
    fi
    ;;
  *)
    echo "Cloud.xxx devenv script 1.0"
    echo ""
    echo "usage: ./devenv.sh [ install | build | configure | unmap | start | delconts | delimgs ]"
    echo ""
    echo "  install [ fresh | copy ] :"
    echo "    setup boot2docker virtual machine properly for shared folders to work"
    echo ""
    echo "      fresh:  create the boot2docker image with guest additions from scratch. WILL take a while"
    echo "      copy:   copy the pre-built boot2docker iso with guest additions installed (try this first)"
    echo ""
    echo "  build:      create the images from the corresponding dockerfiles, could take a while"
    echo "  configure:  forward configuration ports on boot2docker virtual machine"
    echo "  unmap:      undo all port bindings on boot2docker virtual machine"
    echo "  start:      stop any running containers and then start them"
    echo "  delconts:   delete all running/stall containers"
    echo "  delimgs:    delete all images"
    echo ""
esac
