#!/usr/bin/env sh

get_vbox_version(){
    local VER
    VER=$(VBoxManage -v | awk -F "r" '{print $1}')
    if [ -z "$VER" ]; then
        echo "ERROR"
    else
        echo "$VER"
    fi

}

write_vbox_dockerfile(){
    local VER
    VER=$(get_vbox_version)
    if [ ! "$LATEST_RELEASE" = "ERROR" ]; then
        sed "s/\$VBOX_VERSION/$VER/g" boot2docker/Dockerfile.tmpl > boot2docker/Dockerfile
    else
        echo "WUH WOH"
    fi
}

write_vbox_dockerfile
docker build -t="my-boot2docker" boot2docker/
docker run --privileged --name="build-boot2docker" my-boot2docker
rm -f boot2docker/boot2docker.iso
docker cp build-boot2docker:/boot2docker.iso boot2docker/